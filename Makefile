# var
MODULE = $(notdir $(CURDIR))
module = $(shell echo $(MODULE) | tr A-Z a-z)
OS     = $(shell uname -o|tr / _)
NOW    = $(shell date +%d%m%y)
REL    = $(shell git rev-parse --short=4 HEAD)
BRANCH = $(shell git rev-parse --abbrev-ref HEAD)
PEPS   = E26,E302,E305,E401,E402,E701,E702
TARGET = arm-none-eabi

# version
QEMU_VER = 7.0.0-1

# dir
CWD   = $(CURDIR)
TMP   = $(CWD)/tmp
GZ    = $(HOME)/gz

# tool
CURL   = curl -L -o
CF     = clang-format
PY     = $(shell which python3)
PIP    = $(shell which pip3)
PEP    = $(shell which autopep8)
IEX    = iex
MIX    = mix
QEMU   = $(HOME)/bin/xpack-qemu-arm-$(QEMU_VER)/bin/qemu-system-arm

TCC = $(TARGET)-gcc
TLD = $(TARGET)-ld

# src
P += metaL.py $(MODULE).meta.py
S += $(P) rc
X += mix.exs
X += $(shell find lib    -type f -regex .+.ex$$)
X += $(shell find test   -type f -regex .+.ex$$)
X += $(shell find config -type f -regex .+.ex$$)
S += $(X) $(E)
C += $(shell find src -type f -regex .+\\.c$$)
C += $(shell find src -type f -regex .+\\.cpp$$)
H += $(shell find inc -type f -regex .+\\.h$$)
H += $(shell find inc -type f -regex .+\\.hpp$$)
S += $(C) $(H)

PC = tmp/VM.parser.cpp tmp/VM.lexer.cpp
PH = tmp/VM.parser.hpp

# cfg
CFLAGS += -pipe -O0 -g2 -Iinc -Itmp

# all
.PHONY: all
all: bin/hello.elf
	$(QEMU) -M stm32vldiscovery -kernel $<
	$(MAKE) format

bin/%.elf: src/%.c
	$(CC) $(CFLAGS) -o $@ $^

.PHONY: elixir
elixir:
	$(MIX)  compile
	$(MAKE) format

.PHONY: watch
watch:
	$(IEX) -S $(MIX)

.PHONY: gen
gen: $(PY) $(MODULE).meta.py
	$^ && $(MAKE) format

.PHONY: forth
forth: bin/VM config/VM.4th
	$^

# format
.PHONY: format
format: tmp/format_py tmp/format_ex tmp/format_cpp

tmp/format_py: $(P)
	$(PEP) --ignore=$(PEPS) -i $? && touch $@

tmp/format_ex: $(X)
	$(MIX) format && touch $@

tmp/format_cpp: $(C) $(H)
	$(CF) -style=file -i $? && touch $@

# rule
bin/VM: src/VM.cpp $(PC) $(H) $(PH)
	$(CXX) $(CFLAGS) -o $@ $< $(PC)
	$(MAKE) tmp/format_cpp
tmp/VM.parser.cpp: src/VM.yacc
	bison -o $@ $<
tmp/VM.lexer.cpp: src/VM.lex
	flex -o $@ $<

# doc

.PHONY: doxy
doxy: doxy.gen
	rm -rf docs ; doxygen $< 1>/dev/null

.PHONY: doc
doc:
	mkdir -p doc/bib
#	rsync -rv ~/mdoc/stm32book/*     doc/stm32book/

# install
.PHONY: install update updev
install: $(OS)_install doc gz
	$(MAKE) update
	$(MIX)  local.hex
update: $(OS)_update doc gz
	$(PIP) install --user -U pip autopep8 xxhash
	$(MIX) deps.get
updev: update
	sudo apt install -yu `cat apt.dev`

GNU_Linux_install:
GNU_Linux_update:
	sudo apt update
	sudo apt install -yu `cat apt.txt`

gz: qemu

qemu: $(QEMU)
$(QEMU): $(GZ)/xpack-qemu-arm-$(QEMU_VER)-linux-x64.tar.gz
	cd ~/bin ; tar zx < $< && touch $@

XQEMU     = https://github.com/xpack-dev-tools/qemu-arm-xpack
XQEMU_REL = $(XQEMU)/releases/download

$(GZ)/xpack-qemu-arm-$(QEMU_VER)-linux-x64.tar.gz:
	$(CURL) $@ $(XQEMU_REL)/v$(QEMU_VER)/xpack-qemu-arm-$(QEMU_VER)-linux-x64.tar.gz

# merge
MERGE  = Makefile README.md .clang-format doxy.gen $(S)
MERGE += .vscode bin doc config lib inc src tmp test inc
MERGE += apt.dev apt.txt
MERGE += obsidian

dev:
	git push -v
	git checkout $@
	git pull -v
	git checkout shadow -- $(MERGE)
	$(MAKE) doc && git add doc

shadow:
	git push -v
	git checkout $@
	git pull -v

release:
	git tag $(NOW)-$(REL)
	git push -v --tags
	$(MAKE) shadow

ZIP = tmp/$(MODULE)_$(NOW)_$(REL)_$(BRANCH).zip
zip:
	git archive --format zip --output $(ZIP) HEAD
