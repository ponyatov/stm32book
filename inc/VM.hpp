#pragma once

#include <stdint.h>
#include <stdio.h>
#include <assert.h>

#include <iostream>
#include <vector>
#include <map>
using namespace std;

// lexical skeleton
// \ lexer
extern int yylex();
extern int yylineno;
extern char* yytext;
extern FILE* yyin;
// \ parser
extern int yyparse();
extern void yyerror(string msg);
#include "VM.parser.hpp"

#define TOKEN(T, OP)    \
    {                   \
        yylval.op = OP; \
        return T;       \
    }

/// bytecode commands set
enum {
    NOP = 0x00,
    BYE = 0xFF,
    JMP = 0x01,
    qJMP = 0x02,
    CALL = 0x03,
    RET = 0x04,
    LIT = 0x05,
};

/// main memory size, bytes
#define Msz 0x1000
/// return stack size
#define Rsz 0x100
/// data stack size
#define Dsz 0x10

/// unsigned machine word
#define ucell uint16_t
/// signed machine word
#define cell int16_t

/// main memory
extern uint8_t M[Msz];
/// compiler pointer
extern ucell Cp;
/// instruction pointer
extern ucell Ip;

/// memory header
struct HEADER {
    ucell heap;
    ucell entry;
    ucell latest;
} __attribute__((packed));

/// pointer for direct header modifications in M[]
extern HEADER* header;

/// return stack
extern ucell R[Rsz];
/// return stack pointer
extern uint8_t Rp;

/// data stack
extern cell D[Dsz];
/// data stack pointer
extern uint8_t Dp;

/// trace mode: print command on execute
extern bool trace;

/// dump memory `0..Cp`
extern void dump();

/// execute one VM command
extern void step();
/// run interpreter
extern void go();

/// do nothing
extern void nop();
/// stop system
extern void bye();

/// compile byte
extern void C(uint8_t byte);

/// compile cell
extern void C(ucell cell);
extern void C(cell cell);
extern void C(int cell);

/// compile cell as pointer from labels table
extern void C(string sym);

/// label->addr table
extern map<string, cell> addr;
/// reverce addr->label table
extern map<cell, string> label;

/// system initialization
extern void init();

/// unconditional jump
extern void jmp();
/// `( flag -- )` conditional jump on `false`
extern void qjmp();
/// nested call
extern void call();
/// return from nested call
extern void ret();
/// push number literal
extern void lit();
