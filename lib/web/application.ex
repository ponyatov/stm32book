defmodule Web.Application do
  use Application
  require Logger

  def start(_type, _args) do
    port = Application.get_env(:web, :port, 12345)
    Logger.debug(inspect({__MODULE__, " http://127.0.0.1:#{port} "}))

    children = [
      {
        Plug.Cowboy,
        scheme: :http, plug: Web.Router, options: [port: port]
      }
    ]

    opts = [strategy: :one_for_one, name: __MODULE__]
    Supervisor.start_link(children, opts)
  end
end
