defmodule Web.Router do
  use Plug.Router
  use Plug.ErrorHandler

  plug(Plug.Static, at: "/static", from: "static", zip: true)
  plug(Plug.Static, at: "/doc", from: "doc", zip: true)

  plug(:match)
  plug(Plug.Logger)
  plug(:dispatch)

  plug(Plug.Parsers, parsers: [:urlencoded, {:multipart, length: 20}])

  get "/upload" do
    conn = put_resp_content_type(conn, "text/html")
    send_file(conn, 200, "static/upload.html")
  end

  post "/upload" do
    IO.inspect(Plug.Conn.read_body(conn), label: "body")
    send_resp(conn, 201, "Uploaded")
  end

  get "/obsidian/:path" do
    conn = put_resp_content_type(conn, "text/plain")
    send_file(conn, 200, "obsidian/#{path}")
  end

  get "/" do
    conn = put_resp_content_type(conn, "text/html")
    render(conn, "index.html")
  end

  get "/favicon.ico" do
    conn = put_resp_content_type(conn, "image/png")
    send_file(conn, 200, "static/logo.png")
  end

  match _ do
    send_resp(conn, 404, "not found")
  end

  defp render(%{status: status} = conn, template, assigns \\ []) do
    body =
      "templates"
      |> Path.join(template)
      |> String.replace_suffix(".html", ".html.eex")
      |> EEx.eval_file(assigns)

    send_resp(conn, status || 200, body)
  end
end
