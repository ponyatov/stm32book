defmodule Web.MixProject do
  use Mix.Project

  def project do
    [
      app: :web,
      version: "0.0.1",
      elixir: "~> 1.10",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  def application do
    [
      mod: {Web.Application, []},
      env: [port: 46526],
      extra_applications: [
        :logger,
        :cowboy,
        :plug
      ]
    ]
  end

  defp deps do
    [
      {:cowboy, "~> 2.0"},
      {:plug, "~> 1.0"},
      {:plug_cowboy, "~> 2.0"}
    ]
  end
end
