# Application

## `start()`

```Elixir
defmodule Web.Application do
  use Application

  def start(_type, _args) do
    children = []
    opts = [strategy: :one_for_one, name: __MODULE__]
    Supervisor.start_link(children, opts)
  end
end
```

- [[children]] список подчинённых процессов
- `opts` опции для [[супервизор]]а
- [[Supervisor#start_link]]
- [[strategy]] стратегия работы [[супервизор]]а
	- [[strategy#one_for_one]]
	- [[__MODULE__]]