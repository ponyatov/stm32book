# Plug.Parsers.[[JSON]]
-   [`Plug.Parsers.JSON`](https://hexdocs.pm/plug/Plug.Parsers.JSON.html) - parses `application/json` requests with the given `:json_decoder` (can be used as `:json` as well in the `:parsers` option)

