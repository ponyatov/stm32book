# Plug.Parsers.[[MULTIPART]]
-   [`Plug.Parsers.MULTIPART`](https://hexdocs.pm/plug/Plug.Parsers.MULTIPART.html) - parses `multipart/form-data` and `multipart/mixed` requests (can be used as `:multipart` as well in the `:parsers` option)
