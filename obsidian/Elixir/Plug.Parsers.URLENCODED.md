# Plug.Parsers.[[URLENCODED]]

-   [`Plug.Parsers.URLENCODED`](https://hexdocs.pm/plug/Plug.Parsers.URLENCODED.html) - parses `application/x-www-form-urlencoded` requests (can be used as `:urlencoded` as well in the `:parsers` option)
