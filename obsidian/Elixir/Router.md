# Роутер
- https://www.youtube.com/watch?v=uoqGyn00hWA

**Роутер** -- программный компонент бэкенда [[web-приложение|web-приложения]], который
- ставит в соответствие [[URL]] веб-ресурса (без доменной части -- адреса и [[IP-порт]]а) его обработчику
- передаёт [[HTTP]]-запрос функции или методу, который его обрабатывает


## `/lib/web/router.ex`

```elixir
defmodule Web.Router do
  use Plug.Router

  plug(Plug.Static, at: "/static", from: "static", zip: true)

  plug(:match)
  plug(:dispatch)

  get "/" do
    conn = put_resp_content_type(conn, "text/html")
    send_file(conn, 200, "static/index.html")
  end

  get "/favicon.ico" do
    conn = put_resp_content_type(conn, "image/png")
    send_file(conn, 200, "static/logo.png")
  end

  match _ do
    send_resp(conn, 404, "not found")
  end
end
```


**both of the plugs are always required:**

-   The [`:match`](https://github.com/elixir-plug/plug/blob/e9bc0f7cf94ec95189ca5876c2ff67649dce48d9/lib/plug/router.ex#L202-L204) plug is responsible for, well, _matching_ the incoming request to one of the defined routes in the Router.
    
-   The [`:dispatch`](https://github.com/elixir-plug/plug/blob/e9bc0f7cf94ec95189ca5876c2ff67649dce48d9/lib/plug/router.ex#L207-L210) plug is responsible for finally _processing_ the request in the matched route.

