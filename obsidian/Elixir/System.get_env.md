# `System.get_env`
## `get_env/1`

```elixir
IO.puts inspect System.get_env
```

```elixir
%{
	"SHELL" => "/bin/bash", 
	"LANG" => "ru_RU.UTF-8", 
	"PATH" => "/usr/lib/erlang/erts-12.3.1/bin:/usr/lib/erlang/bin:...",
	"_" => "/usr/bin/elixir", 
	"ROOTDIR" => "/usr/lib/erlang", 
	"SHLVL" => "2", "XDG_SESSION_ID" => "3", 
	"PWD" => "/home/ponyatov/hanzi",
	"HOME" => "/home/ponyatov", 
	"USER" => "ponyatov", 
"SSH_AUTH_SOCK" => "/tmp/ssh-cedzSsHpgj0f/agent.1247", "TERM_PROGRAM" => "vscode", 
"ORIGINAL_XDG_CURRENT_DESKTOP" => "undefined", 
	"TERM" => "xterm-256color"
}
```
