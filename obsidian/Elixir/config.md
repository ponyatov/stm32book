# конфигурация

![[Application.get_env]]

## Configuration

https://elixir-lang.org/getting-started/mix-otp/config-and-releases.html#configuration

Конфигурационные файлы предоставляют механизм настройки среды исполнения для любого приложения. [[Elixir]] предоставляет нам две точки настройки:

-   `config/config.exs` - файл читается **до компиляции**, когда нет доступа к исходному коду и зависимостям. В этом файле можно настроить как именно они будут компилироваться.
    
-   `config/runtime.exs` - файл читается после того как приложения и его зависимости были скомпилированы, и поэтому он может настроить как приложение будет работать в рантайме. Если вы хотите считать системные переменные среды через [[System.get_env]],  [[Application.get_env]],  или использовать какой-то механизм внешней конфигурации, этот файл как раз для этого.
