# `mix format`
## автоматическое форматирование [[Elixir]]-кода

```Elixir
# Used by "mix format"
[
  inputs: ["{mix,.formatter}.exs", "{config,lib,test}/**/*.{ex,exs}"]
]
```
