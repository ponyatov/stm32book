# render
```elixir
  defp render(%{status: status} = conn, template, assigns \\ []) do
    body =
      "templates"
      |> Path.join(template)
      |> String.replace_suffix(".html", ".html.eex")
      |> EEx.eval_file(assigns)

    send_resp(conn, status || 200, body)
  end
```
