# web-проект (сервер)

- [[web-приложение]]
- [[mix-проект]]

Для backendа на Elixir нам нужно добавить несколько библиотек, которые позволят сделать наше приложение в виде [[HTTP]]-сервера:

```elixir
defmodule Asta.MixProject do
	use Mix.Project
```
```elixir
  def application do
    [
      mod: {Web.Application, []},
      extra_applications: [
        :logger,
        :cowboy,
        :plug
      ]
    ]
  end
```
- [[logger]]
- [[cowboy]]
- [[plug]]
```elixir
	defp deps do
		[
			{:cowboy, "~> 2.0"},
			{:plug, "~> 1.0"},
			{:plug_cowboy, "~> 2.0"}
		]
	end
end
```

## `/lib/web/application.ex`

```Elixir
defmodule Web.Application do
  use Application
  require Logger

  def start(_type, _args) do
    Logger.debug(inspect({__MODULE__, " http://127.0.0.1:46526 "}))

    children = [
      {
        Plug.Cowboy,
        scheme: :http, plug: Asta.Router, options: [port: 46526]
      }
    ]

    opts = [strategy: :one_for_one, name: __MODULE__]
    Supervisor.start_link(children, opts)
  end
end
```

- `Web.Application` -> [[Application]]
	- [[Logger]]
	- [[Application#start]]

![[Router]]


![[JS-библиотеки]]

![[шаблоны]]

![[Загрузка файлов на сервер]]
