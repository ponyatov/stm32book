# MiniSat
## учебный пикоспутник-конструктор

https://gitlab.com/minisat

- Курсы по работе с конструктором пикоспутника MiniSat
	1. [Введение](https://www.youtube.com/playlist?list=PLhe6g1gTEpdaKxDBnWMuQ6wXwdpQrbrta)

MCU: [[STM32F102C4]]

## настройки [[CubeMX]]

- `System Core` -> `SYS`
	- `Debug` -> `Serial Wire`
- [[GPIO]]
	- `PA8` -> `GPIO_Output`
		- `User Label` -> `LED`

