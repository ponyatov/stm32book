#  `stm32book`
##  [[Cortex-M]] sw/hw hacking and misc projects
### использование микроконтроллеров семейства [[STM32]]/Cortex-M

(c) Dmitry Ponyatov <<dponyatov@gmail.com>> 2022 CC BY-NC

github: https://github.com/ponyatov/stm32book
