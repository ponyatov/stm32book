# OpenSource
## [[Creative Commons]]

Преимущества открытых лицензий тут в том, что вы открываете свою работу более широкой аудитории, позволяете идеям распространяться, и говорите людям, что вы открыты к тому, чтобы они перерабатывали, переводили и адаптировали то, что вы сделали.
