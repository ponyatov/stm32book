# [[STM32]] Development on [[Debian]]

- https://www.theunterminatedstring.com/stm32-development-on-debian/

The [[Ubuntu]] [[VM]] I have been previously using for [[STM32]] development is rapidly falling out of date. Rather than upgrade it I fancied a change (albeit a small one), so I thought I would spin up a [[Debian]] [[VM]]. I figured it would be helpful (to me if to nobody else) to document the steps required to get it up and running.

It you are already familiar with setting up a [[Linux VM]] (or, before anyone interjects a “GNU/Linux” VM), some of the steps below might be a little tedious since I walk through the process. If you are more interesting in installing the dev packages, skip to just after halfway.

This guide will be tailored to configuring a Debian VM for [[C]] language development for the [[STM32F4DISCOVERY]] ([[STM32F407]]) board. If you are using a different board that is supported by [[OpenOCD]] (and [[ChibiOS]] for the demo) hopefully the information here will come close to getting you a suitable environment.

I’m assuming you are somewhat comfortable with a [[Linux]] distribution already, or at the very least eager to learn. If you are a Linux newbie, and found this because you were curious, you still should be able to get set-up with little bother, as it’s mostly copy-paste. However, this is more a walkthrough than a learning tutorial so I would recommend searching about for some training material. I found [LSF101](https://www.edx.org/course/introduction-linux-linuxfoundationx-lfs101x-2) to cover the groundwork pretty well.

## Installing the Guest OS

### Debian [[ISO]]

Obviously Debian will be required for this. An [[ISO]] can be readily downloaded from the [Debian website](https://www.debian.org/). You will find there are multiple versions available, varying not only for [[архитектура процессора|system architecture]] but also with quantity of bundled packages.

I chose the [[amd64]] variant (for my 64-bit host machine) of the version tailored for “Small [[CD-ROM|CD]]s or [[USB]] sticks”. Picking one of the larger versions however may save time during the installation process.

### [[VM]] Software

You could, if so desired install Debian as your main operating system, or even [[dual boot]]. I’ll be running it as a Virtual Machine just for development purposes. I will, somewhat guilty admit that my everyday OS is [[Windows]].

To my knowledge, there are two popular free (as-in-beer) Windows programs which facilitate running a VM:

-   [[VirtualBox]]
-   [[VMware Player]] Workstation Player

Or at least these are the two programs I have used in the past. Each has its own pros and cons. Normally, for a bog standard Linux VM I would choose VirtualBox. In the past however, I have found VirtualBox to be very flaky when it comes to [[USB]] devices (which is obviously a huge inconvenience for programming the [[STM32F4DISCOVERY]]).

It was admittedly some time ago since I last attempted using VirtualBox for [[MCU]] development so it may well be more stable now, but I’m going use [[VMware Player]] here.

