# STM32/`CubeMX`
## генератор кода инициализации микроконтроллеров [[STM32]]

Если вы предпочитаете использовать вместо [[CubeIDE]] другие [[IDE]], вам всё равно понадобится один пакет -- графический **генератор кода инициализации микроконтроллера**.

https://www.st.com/en/development-tools/stm32cubemx.html#get-software

## Начинаем работать в [[CubeMX]]
1. https://habr.com/ru/post/310742/
2. https://habrahabr.ru/post/312810/
3. https://habrahabr.ru/post/323674/

## установка на [[Linux]]

- https://gist.github.com/Lanchon/2156953d18f7534a926b

![[Борьба с санкциями]]

- скачать вручную [en.stm32cubemx_4.23.0.zip](https://drive.google.com/file/d/14TzO-vgsGPNupbB9cXKLBCYB8YYFzM7k/view?usp=sharing) в `~/stm32book/distr`

```sh
$ cd ~/tmp
$ unzip ~/stm32book/distr/en.stm32cubemx-4.23.0.zip
```
```
Archive:  /home/ponyatov/stm32book/distr/en.stm32cubemx-4.23.0.zip
  inflating: SetupSTM32CubeMX-4.23.0.exe  
  inflating: SetupSTM32CubeMX-4.23.0.linux  
   creating: SetupSTM32CubeMX-4.23.0.app/
   creating: SetupSTM32CubeMX-4.23.0.app/Contents/
   creating: SetupSTM32CubeMX-4.23.0.app/Contents/MacOs/
  inflating: SetupSTM32CubeMX-4.23.0.app/Contents/MacOs/SetupSTM32CubeMX-4_23_0_macos  
   creating: SetupSTM32CubeMX-4.23.0.app/Contents/Resources/
  inflating: SetupSTM32CubeMX-4.23.0.app/Contents/Resources/stm32cubemx.icns  
  inflating: SetupSTM32CubeMX-4.23.0.app/Contents/Info.plist  
  inflating: Readme.html             
ponyatov@kot:~/tmp$ _
```
```sh
$ ./SetupSTM32CubeMX-4.23.0.linux
```
![[CubeMX_01.png]]

- Next
- Accept license > Next
- пусть установки: `/home/user/STM32CubeMX` > Next
	- target directory will be created > OK
- Pack installation progress ... > Next
- Done

```
$ cd ~/stm32book/
$ ~/STM32CubeMX/STM32CubeMX
```
![[Pasted image 20220603123759.png]]

- сначала надо поменять пути по умолчанию:
	- нажать `Alt+S` или меню Help > Updater Settings
		- вкладка Updater Settings
			- Repository Folder исправить на 
				- `/home/ponyatov/STM32CubeMX/Repository/`
				- чтобы не засорять домашнюю файловую систему лишними каталогами
			- Check & Update **отключить** в Manual Check
			- Data Auto-Refresh **отключить** в No Auto-Refresh
		- вкладка Connection Parameters
			- No Proxy или настроить Socks Proxy если у вас он есть (через VPN?)
		- OK

В качестве примера создадим проекты-шаблоны для самой простой и дешёвой отладочной платы [[STM32VLDISCOVERY]] и [[MiniSat]] / [[STM32F102C4]]

- New Project

Для серийных отладочных плат в [[CubeMX]] есть режим выбора платы: 
![[STM32VLDISCOVERY]]

- New Project
- вкладка Board Selector
- 


![[MiniSat#настройки CubeMX]]
