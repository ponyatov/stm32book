# STM32CubeIDE

## установка

https://www.st.com/en/development-tools/stm32cubeide.html#get-software

![[Pasted image 20220530170037.png]]

### [[Windows]]

- `STM32CubeIDE-Win`

### [[Linux]]

- `STM32CubeIDE-DEB`закачка в виде .deb-пакета ([[Debian]], [[Ubuntu]], [[Astra Linux]])
- `STM32CubeIDE-Lnx` для остальных дистрибутивов
