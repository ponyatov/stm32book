# STM32CubeMX
## генератор кода инициализации микроконтроллеров [[STM32]]

Если вы предпочитаете использовать вместо [[STM32CubeIDE]] другие [[IDE]], вам всё равно понадобится один пакет -- генератор кода инициализации микроконтроллера.

https://www.st.com/en/development-tools/stm32cubemx.html#get-software

## Начинаем работать в [[STM32CubeMX]]
1. https://habr.com/ru/post/310742/
2. https://habrahabr.ru/post/312810/
3. https://habrahabr.ru/post/323674/
