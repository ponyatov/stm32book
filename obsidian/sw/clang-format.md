# clang-format
## автоматическое форматирование кода

- [[C]]/[[C++]]
- [[JS]] (JavaScript)


## `/.clang-format`

```
# https://clang.llvm.org/docs/ClangFormatStyleOptions.html

BasedOnStyle: Google
IndentWidth: 4
ColumnLimit: 80

SortIncludes: false

AllowShortBlocksOnASingleLine: Always
AllowShortFunctionsOnASingleLine: All

Language: Cpp
```
