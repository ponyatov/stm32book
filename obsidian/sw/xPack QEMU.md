# xPack [[QEMU]]

Существует специализированный форк QEMU, в котором поддержка микроконтроллеров значительно расширена по сравнению с [[qemu-system-arm]] из базовой поставки дистрибутива:

https://github.com/xpack-dev-tools/qemu-arm-xpack/releases

```Makefile
# version
QEMU_VER = 7.0.0-1

# dir
GZ = $(HOME)/gz

# tool
QEMU = $(HOME)/bin/xpack-qemu-arm-$(QEMU_VER)/bin/qemu-system-arm

gz: qemu

qemu: $(QEMU)
	$(QEMU) -M ?
$(QEMU): $(GZ)/xpack-qemu-arm-$(QEMU_VER)-linux-x64.tar.gz
	cd ~/bin ; tar zx < $< && touch $@

XQEMU     = https://github.com/xpack-dev-tools/qemu-arm-xpack
XQEMU_REL = $(XQEMU)/releases/download

$(GZ)/xpack-qemu-arm-$(QEMU_VER)-linux-x64.tar.gz:
	$(CURL) $@ $(XQEMU_REL)/v$(QEMU_VER)/xpack-qemu-arm-$(QEMU_VER)-linux-x64.tar.gz
```

```
$ make qemu
```

```
/home/ponyatov/bin/xpack-qemu-arm-7.0.0-1/bin/qemu-system-arm -M ?

Supported machines are:

lm3s6965evb          Stellaris LM3S6965EVB (Cortex-M3)
lm3s811evb           Stellaris LM3S811EVB (Cortex-M3)
microbit             BBC micro:bit (Cortex-M0)

netduino2            Netduino 2 Machine (Cortex-M3)
netduinoplus2        Netduino Plus 2 Machine (Cortex-M4)

raspi0               Raspberry Pi Zero (revision 1.2)
raspi1ap             Raspberry Pi A+ (revision 1.1)
raspi2b              Raspberry Pi 2B (revision 1.1)

stm32vldiscovery     ST STM32VLDISCOVERY (Cortex-M3)
```
