#include "VM.hpp"

void arg(int i, char *argv[]) {
    //
    printf("argv[%i] = <%s>\n", i, argv[i]);
}

int main(int argc, char *argv[]) {
    printf("\n");
    arg(0, argv);
    init();
    for (int i = 1; i < argc; i++) {
        arg(i, argv);
        assert(yyin = fopen(argv[i], "r"));
        yyparse();
        fclose(yyin);
        header->heap = Cp;
    }
    dump();
    printf("\n");
    go();
}

#define YYERR "\n\n" << yylineno << ": " << msg << " [" << yytext << "]\n\n"
void yyerror(string msg) {
    cerr << YYERR;
    exit(-1);
}

/// main memory
uint8_t M[Msz];
/// compiler pointer
ucell Cp = sizeof(HEADER);
/// instruction pointer
ucell Ip = 0;

HEADER *header = (HEADER *)M;

/// return stack
ucell R[Rsz];
/// return stack pointer
uint8_t Rp = 0;

/// data stack
cell D[Dsz];
/// data stack pointer
uint8_t Dp = 0;

void dump() {
    printf("\n");
    for (uint16_t addr = 0; addr < Cp - 1; addr++) {
        if (addr % 0x10 == 0) printf("\n%.4X: ", addr);
        printf("%.2X ", M[addr]);
        if (addr % 0x10 == 0x07) printf("- ");
    }
    printf("\n");
}

int step_limit = 0x10;
void step() {
    if (!--step_limit) bye();
    // command address
    assert(Ip < Cp);  // Cp < Msz
    if (trace) { printf("\n%.4X %11s:\t", Ip, label[Ip].c_str()); }
    // opcode
    uint8_t op = M[Ip++];
    if (trace) printf("%.2X\t", op);
    // command decode/run
    switch (op) {
        case NOP:
            nop();
            break;
        case BYE:
            bye();
            break;
        case JMP:
            jmp();
            break;
        case qJMP:
            qjmp();
            break;
        case CALL:
            call();
            break;
        case RET:
            ret();
            break;
        case LIT:
            lit();
            break;
        default:
            if (trace) printf("???\n\n");
            exit(-1);
    }
}

void go() {
    while (Ip < Cp) step();
}

void nop() {
    if (trace) printf("nop");
}

void bye() {
    if (trace) printf("bye\n\n");
    exit(0);
}

bool trace = true;

void C(uint8_t byte) {
    M[Cp++] = byte;
    assert(Cp < Msz);
}

void C(int n) {
    *(ucell *)(&M[Cp]) = n;
    Cp += sizeof(ucell);
    assert(Cp < Msz);
}

void C(ucell cell) { C((int)cell); }
void C(cell cell) { C((int)cell); }

map<string, cell> addr;
map<cell, string> label;

void init() {
    label[0x0000] = "_heap";
    label[0x0002] = "_entry";
    label[0x0004] = "_latest";
}

void jmp() {
    Ip = *(uint16_t *)(&M[Ip]);
    assert(Ip < Cp);
    if (trace) { printf("jmp\t%.4X", Ip); }
}

void qjmp() {
    Ip = *(cell *)(&M[Ip]);
    assert(Ip < Cp);
    if (trace) { printf("qjmp\t%.4X", Ip); }
}

void call() {
    R[Rp++] = Ip + sizeof(cell);
    assert(Rp < Rsz);
    Ip = *(cell *)(&M[Ip]);
    assert(Ip < Cp);
    if (trace) { printf("call\t%.4X", Ip); }
}

void ret() {
    if (trace) { printf("ret"); }
    assert(Rp > 0);
    Ip = R[--Rp];
    assert(Ip < Cp);
}

void lit() {
    D[Dp++] = *(cell *)(&M[Ip]);
    Ip += sizeof(cell);
    if (trace) { printf("lit\t%.4X", D[Dp - 1]); }
}
