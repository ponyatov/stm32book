%{
    #include "VM.hpp"    
%}

%option yylineno noyywrap

%%
\\.*        {                   }   // line comment
[ \t\r\n]+  {                   }   // drop spaces
:           { return COLON;     }
;           { return SEMICOLON; }

"nop"       { TOKEN(CMD0, NOP); }
"bye"       { TOKEN(CMD0, BYE); }
"call"      { TOKEN(CMD1, CALL); }

".entry"    { return pENTRY; }

[^ \t\r\n]+ { yylval.s = new string(yytext); return SYM;  }

.           { yyerror("lexer"); }   // any undetected char
