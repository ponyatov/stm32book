%{
    #include "VM.hpp"
%}

%defines %union { char c; string *s; uint8_t op; }

%token<c>   CHAR
%token<s>   SYM
%token      COLON SEMICOLON
%token<op>  CMD0 CMD1
%token      pENTRY

%%
REPL :
     | REPL CHAR        { cout << $2 << endl; }
     | REPL COLON SYM   { header->latest = Cp; // LFA
                          addr[*$3] = Cp; label[Cp] = *$3;
                          Ip = header->entry = Cp; } // CFA
     | REPL SEMICOLON   { C(RET); }
     | REPL CMD0        { C($2); }
     | REPL CMD1 SYM    { C($2); C(addr[*$3]); }
     | REPL pENTRY SYM  { Ip = header->entry = addr[*$3]; }
